class DesinteresScene extends Scene {

  int size = 50;
  DesinteresScene() {
    super();

    for (int i = 0; i < 15; i++) {
      Entity e;

      if (i == 0) {
        e = new Entity()
          .setSize(size)
          .setPosition(width / 2, height / 2)
          .setColor(ACCENT)
          .setSize(30);

        e.getBody().setDensity(0.1);
      } else {
        e = new Entity()
          .setSize(size)
          .setPosition(random(left + size * 2, right - size * 2), random(top + size * 2, bottom - size * 2))
          .setMinVelocity(100);

        e.getBody().setDensity(1000000);
      }

      entities.add(e);
    }

    entities.get(0).setColor(ACCENT).setGrabbable(true);

    addAll();
  }
}
