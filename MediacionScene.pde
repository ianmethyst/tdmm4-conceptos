class MediacionScene extends Scene {

  FCompound top;
  Wall middleWall;
  InvisibleWall bridgeRoof;
  Background bridge, topBG, bottomBG;

  int cornerWidth = (width - boundariesWidth * 2) / 3;
  int cornerHeight = (height - boundariesWidth * 2) / 3;

  MediacionScene() {
    super();

    Wall tlWall = new Wall(boundariesWidth + cornerWidth / 2, boundariesWidth + cornerHeight / 2, cornerWidth, cornerHeight);
    Wall trWall = new Wall(width - boundariesWidth - cornerWidth / 2, boundariesWidth + cornerHeight / 2, cornerWidth, cornerHeight);
    Wall blWall = new Wall(boundariesWidth + cornerWidth / 2, height - boundariesWidth - cornerHeight / 2, cornerWidth, cornerHeight);
    Wall brWall = new Wall(width - boundariesWidth - cornerWidth / 2, height - boundariesWidth - cornerHeight / 2, cornerWidth, cornerHeight);

    addWall(tlWall);
    addWall(trWall);
    addWall(blWall);
    addWall(brWall);

    for (int i = 0; i < 5; i++) {
      entities.add(new Entity()
        .setPosition(width * 0.33 + boundariesWidth, height / 2)
        .setVertices(5)
        .setMinVelocity(150));

      entities.add(new Entity()
        .setPosition(width * 0.66 + boundariesWidth, height / 2)
        .setVertices(3)
        .setMinVelocity(150));
    }

    //Wall bottomMediator = new Wall(width / 2, height - boundariesWidth - cornerHeight / 2, cornerWidth, cornerHeight);
    //Wall topMediator = new Wall(width / 2, boundariesWidth + cornerHeight / 2, cornerWidth, cornerHeight);

    middleWall = new Wall(width / 2, height / 2, cornerWidth, cornerHeight + 2);
    middleWall.getBox().setFillColor(ACCENT);
    middleWall.getBox().setStatic(true);
    middleWall.getBox().setGrabbable(false);

    topBG = new Background(width / 2, height / 2 - cornerHeight - 1, cornerWidth + 2, cornerHeight + 2);
    topBG.setColor(ALTERNATIVE);

    bottomBG = new Background(width / 2, height / 2 + cornerHeight + 1, cornerWidth + 2, cornerHeight + 2);
    bottomBG.setColor(ALTERNATIVE);

    bridge = new Background(width / 2, height / 2 - cornerHeight, cornerWidth, cornerHeight);
    bridge.setColor(ACCENT);
    
    bridgeRoof = new InvisibleWall(width / 2, height / 2 - cornerHeight * 2, cornerWidth, cornerHeight);

    addBackground(topBG);
    addBackground(bottomBG);
    addBackground(bridge);

    addAll();
    addWall(bridgeRoof);
    addWall(middleWall);
    //addWall(bottomMediator);
  }

  void update() {
    super.update();
    //topBG.display();
    //bottomBG.display();
    //bridge.display();

    if (mousePressed) {
      bridge.setPosition(width / 2, mouseY);
    }

    float y = bridge.getPosition().y;

    if (y < boundariesWidth + cornerHeight / 2) {
      bridge.setPosition(width / 2, boundariesWidth + cornerHeight / 2);
    } else if (y > height / 2) {
      bridge.setPosition(width / 2, height / 2);
    } else {
      bridge.setPosition(width / 2, y);
    }

    //middleWall.getBox().setRotation(0);
    bridgeRoof.setPosition(width / 2, bridge.getPosition().y - cornerHeight);
    middleWall.setPosition(width / 2, bridge.getPosition().y + cornerHeight);
  }
}
