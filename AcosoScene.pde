class AcosoScene extends Scene {
  public AcosoScene() {
    super();

    int size = 45;

    entities.add(new Entity()
      .setPosition(random(left + size * 2, right - size * 2), random(top + size * 2, bottom - size * 2))
      .setVertices(5)
      .setColor(ACCENT)
      .setGrabbable(true));

    for (int i = 0; i < 10; i++) {
      entities.add(new Entity()
        .setPosition(random(left + size * 2, right - size * 2), random(top + size * 2, bottom - size * 2))
        .setSize(size));
    }

    addAll();
  }

  public void update() {
    for (Entity e : entities) {
      if (e == entities.get(0)) {
        continue;
      }
      entities.get(0).attract(e);
    }

    super.update();
  }
}
