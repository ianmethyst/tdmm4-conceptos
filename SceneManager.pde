class SceneManager {
  private Scene scenes[];
  private Scene currentScene;
  private int currentSceneIndex;

  float ratio;

  boolean overview;

  public SceneManager(float ratio) {
    this.ratio = ratio;

    overview = true;

    scenes = new Scene[12];

    scenes[0] = new AcosoScene();
    scenes[1] = new VanidadScene();
    scenes[2] = new ExtranjerizacionScene();
    scenes[3] = new LiderazgoScene();
    scenes[4] = new MediacionScene();
    scenes[5] = new IndividualismoScene();
    scenes[6] = new AlienacionScene();
    scenes[7] = new DesinteresScene();
    scenes[8] = new DesarraigoScene();
    scenes[9] = new XenofobiaScene();
    scenes[10] = new DesigualdadScene();
    scenes[11] = new EnfermedadScene();

    currentSceneIndex = 0;
    currentScene = scenes[currentSceneIndex];
  }

  public SceneManager() {
    this(4/3);
  }

  public void display() {
    if (overview) {

      int y = 0;
      int x = 0;

      for (int i = 0; i < scenes.length; i++) {
        if (i != 0) {
          if (i % 4 == 0) {
            y += height / 3;
            x = 0;
          } else { 
            x += width / 4;
          }
        }

        scenes[i].renderOffScreen(x, y, width / 4, height / 3);

        if (currentSceneIndex != i) {
          pushStyle();
          fill(0, 100);
          rect(x, y, width / 4, height / 3);
          popStyle();
        }
      }
    } else {
      currentScene.display();
    }
  }

  public void update() {
    if (overview) {
      for (Scene s : scenes ) {
        s.update();
      }

      selectSceneByMouse();
    } else { 
      currentScene.update();
    }
  }

  public void setCurrentScene(int s) {
    if (s < 0 || s >= scenes.length) {
      println("ERROR! scene number not in range");
    } else {
      currentSceneIndex = s;
      currentScene = scenes[currentSceneIndex];
    }
  }

  public int getCurrentScene() {    
    return currentSceneIndex;
  }

  public void selectNextScene() {
    if (getCurrentScene() + 1 >= scenes.length) {
      setCurrentScene(0);
    } else {
      setCurrentScene(getCurrentScene() + 1);
    }
  }

  public void selectPreviousScene() {
    if (getCurrentScene() - 1 < 0) {
      setCurrentScene(scenes.length - 1);
    } else {
      setCurrentScene(getCurrentScene() - 1);
    }
  }

  private void selectSceneByMouse() {
    int x = int(map(mouseX, 0, width, 0, 4));
    int y = int(map(mouseY, 0, height, 0, 3));

    setCurrentScene(y * 4 + x);
  }

  public void toggleOverview() {
    overview = !overview;
  }

  public void setOverview(boolean state) {
    overview = state;
  }

  public boolean getOverview() {
    return overview;
  }
}
