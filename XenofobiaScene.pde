class XenofobiaScene extends Scene {  
  int BOTTOM_BITS = 0x0001;
  int TOP_BITS = 0x0002;
  int SEP_BITS = 0x0003;

  int size = 30;

  Entity firstAttractor;

  XenofobiaScene() {
    super();

    for (Wall w : walls) {
      w.setMaskBits(BOTTOM_BITS | TOP_BITS);
    }

    Wall sep = new Wall(width / 2, height / 2, width, 6);
    sep.setCategoryBits(SEP_BITS);
    sep.setMaskBits(TOP_BITS);

    addWall(sep);

    for (int i = 0; i < 6; i++) {
      Entity e = new Entity()
        .setSize(30)
        .setPosition(random(left + size, right - size * 2), random(top + size * 2, height / 2 - size * 2))
        .setVertices(5)
        .setCategoryBits(TOP_BITS)
        .setMaskBits(BOTTOM_BITS | TOP_BITS)
        .setMinVelocity(175);

      entities.add(e);

      entities.add(createDraggableEntity());
    }

    addAll();
  }

  private Entity createDraggableEntity() {
    Entity d = new Entity()
      .setGrabbable(true)
      .setSize(30)
      .setPosition(random(left + size * 2, right - size * 2), random(height / 2 + size * 2, bottom - size * 2))
      .setColor(ACCENT)
      .setCategoryBits(BOTTOM_BITS)
      .setMaskBits(BOTTOM_BITS | TOP_BITS);

    return d;
  }

  void update() {
    super.update();

    ListIterator<Entity> iterator;
    iterator = entities.listIterator();

    while (iterator.hasNext()) {
      Entity e = iterator.next();

      if (e.getCategoryBits() == BOTTOM_BITS && e.getPosition().y < height / 2 - e.getSize()) {
        e.setCategoryBits(TOP_BITS);
      }

      if (e.getColor() == ACCENT && e.getCategoryBits() == TOP_BITS && firstAttractor == null) {
        firstAttractor = e;
      }

      if (e.getAlpha() < 0) {
        firstAttractor = null;
        iterator.remove();
        iterator.add(createDraggableEntity());
      }
    }
    
    for (Entity e : entities) {
      if (firstAttractor != null) {
        if ( e == firstAttractor) {
          int contacts = e.getBody().getContacts().size();
          int alpha = e.getAlpha();
          e.setAlpha(alpha - contacts);

          continue;
        }

        if (e.getColor() == PRIMARY) {
          e.setMinVelocity(5);
          firstAttractor.attract(e);
        }
      } else {
        if (e.getColor() == PRIMARY) {
          e.setMinVelocity(175);
        }
      }
    }
  }
}
