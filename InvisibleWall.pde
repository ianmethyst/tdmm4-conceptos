class InvisibleWall extends Wall {
  public InvisibleWall(float px, float py, float sx, float sy) {
    super(px, py, sx, sy);
  }
  
  void display() {
    
  }
  
  void renderOffscreen() {
    
  }
}
