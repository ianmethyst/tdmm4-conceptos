class IndividualismoScene extends Scene {
  public IndividualismoScene() {
    super();

    for (int i = 0; i < 5; i++) {
      float angle = map(i, 0, 5, 0, -TWO_PI);

      float x = cos(angle) * height / 5;
      float y = sin(angle) * height / 5;

      entities.add(new Entity()
        .setPosition(width / 2 + x, height / 2 + y)
        .setCollisionGroup(-1));
    }

    entities.get(0).setColor(ACCENT).setGrabbable(true);

    addAll();
  }

  void update() {
    super.update();

    for (Entity e : entities) {
      if (e == entities.get(0)) {
        continue;
      }

      // Interactive entity velocity;
      FBody b = entities.get(0).getBody();
      PVector vel = new PVector(b.getVelocityX(), b.getVelocityX());
      float abs = (abs(vel.x) + abs(vel.y));
      
      // Individual entities

      e.setMinVelocity(abs);
    }
  }
}
