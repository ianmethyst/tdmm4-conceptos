class ExtranjerizacionScene extends Scene {  
  int BOTTOM_BITS = 0x0001;
  int TOP_BITS = 0x0002;
  int SEP_BITS = 0x0003;

  int size = 30;

  ExtranjerizacionScene() {
    super();

    for (Wall w : walls) {
      w.setMaskBits(BOTTOM_BITS | TOP_BITS);
    }

    Wall sep = new Wall(width / 2, height / 2, width, 6);
    sep.setCategoryBits(SEP_BITS);

    addWall(sep);

    for (int i = 0; i < 10; i++) {
      Entity e = new Entity()
        .setSize(30)
        .setPosition(random(left + size, right - size * 2), random(height / 2 + size * 2, bottom - size * 2))
        .setMinVelocity(175);

      entities.add(e);
      addEntity(e);

      entities.add(createDraggableEntity());
    }

    addAll();
  }

  private Entity createDraggableEntity() {
    Entity d = new Entity()
      .setGrabbable(true)
      .setSize(30)
      .setPosition(random(left + size * 2, right - size * 2), random(top + size * 2, height / 2 - size * 2))
      .setColor(ACCENT)
      .setCategoryBits(TOP_BITS)
      .setMaskBits(BOTTOM_BITS | TOP_BITS);

    addEntity(d);

    return d;
  }

  void update() {
    super.update();

    ListIterator<Entity> iterator;
    iterator = entities.listIterator();

    int add = 0;

    while (iterator.hasNext()) {
      Entity e = iterator.next();

      if (e.getPosition().y < (height / 2 - e.getSize()) && e.getCategoryBits() == BOTTOM_BITS && e.getMaskBits() == (BOTTOM_BITS | TOP_BITS)) {
        e.setGrabbable(true);
        e.setMinVelocity(-1);
        e.setCategoryBits(TOP_BITS);
      }

      if (e.getPosition().y > (height / 2 + e.getSize()) && e.getCategoryBits() == TOP_BITS) {
        e.setGrabbable(false);
        e.setMinVelocity(175);
        e.setCategoryBits(BOTTOM_BITS);
        add++;
      }
    }

    if (add > 0) {
      for (int i = 0; i < add; i++) {
        addEntity(createDraggableEntity());
      }
    }
  }
}
