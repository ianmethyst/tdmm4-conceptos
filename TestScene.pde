class TestScene extends Scene {
  public TestScene() {
    super();

    walls.add(new Wall(25, height / 2, 50, height));
    walls.add(new Wall(width - 25, height / 2, 50, height));
    walls.add(new Wall(width / 2, 25, width, 50));
    walls.add(new Wall(width / 2, height - 25, width, 50));

    walls.add(new Wall(width /2, height /2, 2, height));


    entities.add(new Entity().setMinVelocity(200));
    entities.add(new Entity().setGrabbable(true).setColor(ACCENT));

    addAll();
  }
}
