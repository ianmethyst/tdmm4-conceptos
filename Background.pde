class Background {
  PVector size;
  PVector position;
  color colour;

  public Background(float px, float py, float sx, float sy) {
    position = new PVector(px, py);
    size = new PVector(sx, sy);
    colour = BG;
  }

  public void display() {
    push();

    rectMode(CENTER);
    fill(colour);

    translate(position.x, position.y);

    rect(0, 0, size.x, size.y); 

    pop();
  }


  public void renderOffscreen(PGraphics p) {
    p.push();

    p.rectMode(CENTER);
    p.fill(colour);

    p.translate(position.x, position.y);

    p.rect(0, 0, size.x, size.y); 

    p.pop();
  }

  public PVector getPosition() {
    return position;
  }

  public void setPosition (PVector v) {
    position.set(v);
  }

  public void setPosition(float x, float y) {
    position.set(x, y);
  }

  public void setColor(color c) {
    colour = c;
  }
}
