class DesarraigoScene extends Scene {
  Entity li, ri;

  int leftVertices = 4;
  int rightVertices = 6;

  int size = 30;

  PVector leftCenter = new PVector(width * 0.25, height * 0.5);
  PVector rightCenter = new PVector(width * 0.75, height * 0.5);

  DesarraigoScene() {
    super();

    for (Wall w : walls) {
      w.setMaskBits(0x0001 | 0x0002);
    }

    Wall sep = new Wall(width / 2, height / 2, 6, height);
    addWall(sep);

    for (int i = 0; i < 9; i++) {
      entities.add(new Entity()
        .setPosition(random(left + size * 2, width / 2 - size), random(top + size * 2, bottom - size * 2))
        .setVertices(leftVertices)
        .setMinVelocity(100)
        .setMaskBits(0x0001 | 0x0002));

      entities.add(new Entity()
        .setPosition(random(width / 2 + size * 2, right - size * 2), random(top + size * 2, bottom - size * 2))
        .setVertices(rightVertices)
        .setMinVelocity(100)
        .setMaskBits(0x0001 | 0x0002));
    }

    li = new Entity()
      .setColor(ACCENT)
      .setGrabbable(true)
      .setPosition(width * 0.25, height / 2)
      .setVertices(leftVertices)
      .setMaskBits(0x0001)
      .setCategoryBits(0x0002);

    ri = new Entity()
      .setColor(ACCENT)
      .setGrabbable(true)
      .setPosition(width * 0.75, height / 2)
      .setVertices(rightVertices)
      .setMaskBits(0x0001)
      .setCategoryBits(0x0002);

    ri.getBody().setRestitution(1);
    li.getBody().setRestitution(1);

    entities.add(li);
    entities.add(ri);

    addAll();
  }

  void update() {
    super.update();

    if (li.getPosition().x > width / 2) {
      if (li.getCategoryBits() != 0x0001) {
        li.setCategoryBits(0x0001);
        li.setMinVelocity(100);
      }
      attractEntityTo(li, leftCenter);
    }

    if (ri.getPosition().x < width / 2) {
      if (ri.getCategoryBits() != 0x0001) {
        ri.setCategoryBits(0x0001);
        ri.setMinVelocity(100);
      }
      attractEntityTo(ri, rightCenter);
    }
  }

  void attractEntityTo(Entity e, PVector t) {
    PVector position = e.getPosition(); 
    PVector acceleration = PVector.sub(t, position);
    // Set magnitude of acceleration
    acceleration.mult(50);

    //e.body.setTransform(e.body.getWorldCenter(), atan2(target.y, target.x));
    e.getBody().addTorque(acceleration.heading());
    e.getBody().addForce(acceleration.x, acceleration.y);
  }
}
