class VanidadScene extends Scene {
  Entity i;
  Entity r;

  Background bg;

  VanidadScene() {
    super();

    walls.add(new InvisibleWall(width / 2, height / 2, 3, height));

    bg = new Background(width * 0.75, height / 2, width /2, height);
    bg.setColor(ALTERNATIVE);

    addBackground(bg);


    r = new Entity()
      .setPosition(width * 0.75, height * 0.5)
      .setVertices(7)
      .setSize(60)
      .setColor(BG)
      .setCategoryBits(0x0002)
      .setMaskBits(0x0002);

    entities.add(r);

    i = new Entity()
      .setPosition(width * 0.25, height * 0.5)
      .setVertices(4)
      .setColor(ACCENT)
      .setGrabbable(true);

    entities.add(i);

    addAll();
  }

  public void update() {
    PVector p = i.getPosition();
    float a = i.getRotation();

    float m = map(p.x, 0.0, width / 2, width, width /2);

    r.setPosition(m, p.y);
    r.setRotation(a);

    super.update();
  }
}
