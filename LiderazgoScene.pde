class LiderazgoScene extends Scene {
  int DEFAULT_BITS = 0x0001;
  int LEADER_BITS = 0x0002;

  int TOP_LEFT = 0;
  int TOP_RIGHT = 1;
  int BOTTOM_LEFT = 2;
  int BOTTOM_RIGHT = 3;

  int currentCuadrant = 0;

  int size = 30;

  Entity a;

  public LiderazgoScene() {
    super();

    for (Wall w : walls) {
      w.setMaskBits(DEFAULT_BITS | LEADER_BITS);
    }

    Wall hSep = new Wall(width / 2, height / 2, width, 6);
    Wall vSep = new Wall(width / 2, height / 2, 6, height);
    addWall(hSep);
    addWall(vSep);

    a = new Entity()
      .setPosition(width * 0.25, height * 0.25)
      .setColor(ACCENT)
      .setCategoryBits(LEADER_BITS)
      .setMaskBits(DEFAULT_BITS)
      .setSize(size * 1.5)

      .setGrabbable(true);

    for (int i = 0; i < 6; i++) {
      entities.add(new Entity()
        .setPosition(random(left + size * 2, right - size * 2), random(top + size * 2, bottom - size * 2))
        .setSize(size)
        .setMaskBits(DEFAULT_BITS)
        .setSize(size)
        .setMinVelocity(175));
    }
    
    entities.add(a);


    addAll();
  }

  public void update() {
    super.update();

    PVector p = a.getPosition();
    if (p.x < width / 2 && p.y < height / 2) {
      currentCuadrant = TOP_LEFT;
    } else if (p.x > width / 2 && p.y < height / 2) {
      currentCuadrant = TOP_RIGHT;
    } else if (p.x < width / 2 && p.y > height / 2) {
      currentCuadrant = BOTTOM_LEFT;
    } else if (p.x > width / 2 && p.y > height / 2) {
      currentCuadrant = BOTTOM_RIGHT;
    }    

    for (Entity e : entities) {
      if (e == a) {
        continue;
      }

      PVector pe = e.getPosition();
      int c = -1;

      if (pe.x < width / 2 && pe.y < height / 2) {
        c = TOP_LEFT;
      } else if (pe.x > width / 2 && pe.y < height / 2) {
        c = TOP_RIGHT;
      } else if (pe.x < width / 2 && pe.y > height / 2 ) {
        c = BOTTOM_LEFT;
      } else if (pe.x > width / 2  && pe.y > height / 2 ) {
        c = BOTTOM_RIGHT;
      }

      if (c != currentCuadrant && e.getCategoryBits() == DEFAULT_BITS) {
        e.setMinVelocity(-1);
        e.setCategoryBits(LEADER_BITS);
      } else if (c == currentCuadrant && e.getCategoryBits() == LEADER_BITS) {
        e.setCategoryBits(DEFAULT_BITS);
        e.setMinVelocity(175);
      }

      if (e.getCategoryBits() == LEADER_BITS) {
        a.attract(e);
      }
    }
  }
}
