class AlienacionScene extends Scene {
  Entity i;

  int leftVertices = 3;
  int rightVertices = 7;

  int size = 30;

  int lastSeconds;

  AlienacionScene() {
    super();

    for (Wall w : walls) {
      w.setMaskBits(0x0001 | 0x0002);
    }

    Wall sep = new Wall(width / 2, height / 2, 6, height);
    addWall(sep);

    for (int i = 0; i < 10; i++) {
      entities.add(new Entity()
        .setPosition(random(left + size * 2, width / 2 - size), random(top + size * 2, bottom - size * 2))
        .setVertices(leftVertices)
        .setMinVelocity(100)
        .setMaskBits(0x0001 | 0x0002));


      entities.add(new Entity()
        .setPosition(random(width / 2 + size * 2, right - size * 2), random(top + size * 2, bottom - size * 2))
        .setVertices(rightVertices)
        .setMinVelocity(100)
        .setMaskBits(0x0001 | 0x0002));
    }

    i = new Entity()
      .setColor(ACCENT)
      .setGrabbable(true)
      .setPosition(width / 4, height / 2)
      .setVertices(leftVertices)
      .setMaskBits(0x0001)
      .setCategoryBits(0x0002);
      
    entities.add(i);

    addAll();
  }

  void update() {
    super.update();

    if (second() > lastSeconds) {
      if (i.getPosition().x < width / 2) {
        if (i.getVertices() > leftVertices) {
          i.setVertices(i.getVertices() - 1);
          world.add(i.getBody());
        }
      } else {
        if (i.getVertices() < rightVertices) {
          i.setVertices(i.getVertices() + 1);
          addEntity(i);
        }
      }
    }

    lastSeconds = second();
  }
}
