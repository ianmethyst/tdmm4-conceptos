class Scene {
  ArrayList<Entity> entities;
  ArrayList<Wall> walls;
  ArrayList<Background> backgrounds;

  PGraphics graphics;

  FWorld world;

  PVector position;

  int boundariesWidth = 50;
  int left = boundariesWidth;
  int right = width - boundariesWidth;
  int top = boundariesWidth;
  int bottom = height - boundariesWidth;

  public Scene() {
    backgrounds = new ArrayList<Background>(0);
    entities = new ArrayList<Entity>(0);
    walls = new ArrayList<Wall>(0);

    world = new FWorld();
    world.setGravity(0, 0);
    world.setEdges(BG);

    graphics = createGraphics(width, height);

    createBoundaries();
  }

  public void createBoundaries() {
    walls.add(new Wall(boundariesWidth / 2, height / 2, boundariesWidth, height));
    walls.add(new Wall(width - boundariesWidth / 2, height / 2, boundariesWidth, height));
    walls.add(new Wall(width / 2, boundariesWidth / 2, width, boundariesWidth));
    walls.add(new Wall(width / 2, height - boundariesWidth / 2, width, boundariesWidth));
  }

  public void addAll() {
    for (Wall w : walls) {
      world.add(w.getBox());
    }

    for (Entity e : entities) {
      world.add(e.getBody());
    }
  }

  public void addBackground(Background b) {
    backgrounds.add(b);
  }

  public void addWall(Wall w) {
    walls.add(w);
    world.add(w.getBox());
  }

  public void addEntity(Entity e) {
    entities.add(e);
    world.add(e.getBody());
  }

  public void display() {
    for (Background b : backgrounds) {
      b.display();
    }

    for (Entity e : entities) {
      e.render();
    }

    for (Wall w : walls) {
      w.render();
    }
  }

  public void renderOffScreen(int px, int py, int sx, int sy) {
    graphics.beginDraw();
    graphics.background(BG);

    for (Background b : backgrounds) {
      b.renderOffscreen(graphics);
    }

    for (Entity e : entities) {
      e.renderOffscreen(graphics);
    }

    for (Wall w : walls) {
      w.renderOffscreen(graphics);
    }

    graphics.endDraw();

    image(graphics, px, py, sx, sy);
  }; 
  public void update() {
    world.step();

    for (Entity e : entities) {
      if (e.needsUpdate) {

        boolean grabbed = false;

        if (world.getMouseJoint().getGrabbedBody() == e.getBody()) {
          //e.setGrabbable(false);
          //world.getMouseJoint().releaseGrabbedBody();
          grabbed = true;
        }

        world.remove(e.getBody());
        e.create();
        world.add(e.getBody());

        if (grabbed) {
          e.setGrabbable(true);
          //println(world.getMouseJoint());
          //println(e.getBody());

          //world.getMouseJoint().setGrabbedBodyAndTarget(e.getBody(), mouseX, mouseY);
        }
      }

      e.update();
    }
  }
}
