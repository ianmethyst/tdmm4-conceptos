class Entity {
  protected FPoly poly;
  protected PShape shape;

  protected float size;
  protected int vertices;
  protected PVector position;
  protected boolean dynamic;
  protected boolean grabbable;
  protected float minVelocity;

  protected color colour;
  protected int alpha;
  protected boolean appearing;
  protected float enfermedadLerpValue;

  // Collision filtering
  protected int collisionGroup; 
  protected int categoryBits;
  protected int maskBits;

  // Use this to recreate shape on size or vertices number change
  protected boolean needsUpdate;

  public Entity() {
    grabbable = false;
    vertices = 3;
    size = 30;
    colour = PRIMARY;
    position = new PVector(random(0, width), random(0, height));
    minVelocity = -1;

    enfermedadLerpValue = 0;

    maskBits = 0x0001;
    categoryBits = 0x0001;
    collisionGroup = 0;

    alpha = 0;
    appearing = true;

    needsUpdate = true;
  }

  public void create() {
    if (poly == null) {
      poly = new FPoly();
    }

    shape = createShape();

    poly.setDrawable(false); // Disable fisica shape drawing
    poly.setGrabbable(grabbable);
    poly.setDensity(10);
    poly.setRestitution(0.1);
    poly.setDamping(0);
    poly.setFriction(0);
    poly.setGroupIndex(collisionGroup);
    poly.setFilterBits(maskBits);
    poly.setCategoryBits(categoryBits);
    poly.setStatic(false);
    poly.setPosition(position.x, position.y);

    // Set the vertices position
    shape.beginShape();
    for (int i = 0; i < vertices; i++) {
      float angle = map(i, 0, vertices, 0, -TWO_PI);

      float x = cos(angle) * size;
      float y = sin(angle) * size;

      poly.vertex(x, y);
      shape.vertex(x, y);
    }

    shape.endShape();
    shape.disableStyle();

    needsUpdate = false;
  }


  public void update() {
    if (appearing && alpha < 255) {
      alpha += 5;
    }

    if ( alpha >= 255) {
      appearing = false;
    }

    position.set(poly.getX(), poly.getY());

    if (minVelocity > 0) {
      constrainMinVelocity();
    }
  }

  public void render() {
    push();

    translate(position.x, position.y);
    rotate(getRotation());

    fill(colour, alpha);
    shape(shape, 0, 0);

    pop();
  }

  public void renderOffscreen(PGraphics p) {
    p.push();

    p.translate(position.x, position.y);
    p.rotate(getRotation());

    p.noStroke();

    p.fill(colour, alpha);
    p.shape(shape, 0, 0);

    p.pop();
  }

  private void constrainMinVelocity() {
    float velX = poly.getVelocityX();
    float velY = poly.getVelocityY();

    if (velX == 0 && velY == 0) {
      velX = random(-1, 1);
      velY = random(-1, 1);
    }

    float abs = (abs(velX) + abs(velY));

    if (abs < minVelocity) {
      float distance = minVelocity - abs;

      float percentX = abs(velX) / abs;
      float percentY = abs(velY) / abs;

      if (velX < 0) {
        percentX *= -1;
      }

      if (velY < 0) {
        percentY *= -1;
      }

      velX += distance * percentX;
      velY += distance * percentY;

      poly.setVelocity(velX, velY);
    }
  }

  public void attract(Entity e) {
    PVector acceleration = PVector.sub(position, e.getPosition());
    // Set magnitude of acceleration
    acceleration.mult(50);

    //e.body.setTransform(e.body.getWorldCenter(), atan2(target.y, target.x));
    e.getBody().addTorque(acceleration.heading());
    e.getBody().addForce(acceleration.x, acceleration.y);
  }

  // Getters

  public FBody getBody() {
    if (poly == null) {
      create();
    }

    return poly;
  }

  public int getVertices() {
    return vertices;
  }

  public float getSize() {
    return size;
  }

  public boolean needsUpdate() {
    return needsUpdate;
  }

  public PVector getPosition() {
    return position;
  }

  public float getRotation() {
    return poly.getRotation();
  }

  public color getColor() {
    return colour;
  }

  public int getCollisionGroup() {
    // https://www.iforce2d.net/b2dtut/collision-filtering
    return collisionGroup;
  }

  public int getCategoryBits() {
    // https://www.iforce2d.net/b2dtut/collision-filtering
    return categoryBits;
  }

  public int getMaskBits() {
    // https://www.iforce2d.net/b2dtut/collision-filtering
    return maskBits;
  }

  public float getMinVelocity() {
    return minVelocity;
  }

  public int getAlpha() {
    return alpha;
  }

  public float getEnfermedadLerpValue() {
    return enfermedadLerpValue;
  }

  // Setters 
  public Entity setGrabbable(boolean b) {
    grabbable = b;

    if (poly != null) {
      poly.setGrabbable(grabbable);
    }

    return this;
  }

  public Entity setSize(float s) {
    size = s;
    needsUpdate = true;

    return this;
  }

  public Entity setVertices(int v) {
    vertices = v;
    needsUpdate = true;

    return this;
  }

  public Entity setPosition(PVector v) {
    position.set(v.x, v.y);

    if (poly != null) {
      poly.setPosition(v.x, v.y);
    }
    return this;
  }

  public Entity setPosition(float x, float y) {
    return this.setPosition(new PVector(x, y));
  }


  public Entity setColor(color c) {
    colour = c;
    return this;
  }

  public Entity setRotation(float r) {
    poly.setRotation(r);
    return this;
  }

  public Entity setCollisionGroup(int g) {
    // https://www.iforce2d.net/b2dtut/collision-filtering
    collisionGroup = g;

    if (poly != null) {
      poly.setGroupIndex(collisionGroup);
    }

    return this;
  }

  public Entity setCategoryBits(int b) {
    categoryBits = b;

    if (poly != null) {
      poly.setCategoryBits(categoryBits);
    }

    return this;
  }

  public Entity setMaskBits(int b) {
    maskBits = b;

    if (poly != null) {
      poly.setFilterBits(maskBits);
    }

    return this;
  }

  public Entity setMinVelocity(float v) {
    minVelocity = v;
    return this;
  }

  public Entity setAlpha(int a) {
    alpha = a;
    return this;
  }

  public Entity setEnfermedadLerpValue(float v) {
    enfermedadLerpValue = v;
    return this;
  }
}
