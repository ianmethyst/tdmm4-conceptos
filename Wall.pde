class Wall {
  protected FBox box;

  protected PVector position;
  protected PVector size;

  protected int categoryBits;
  protected int maskBits;
  protected int collisionGroup;

  protected color colour;

  public Wall(float px, float py, float sx, float sy) {
    position = new PVector(px, py);
    size = new PVector(sx, sy);
    
    colour = ALTERNATIVE;
    
    maskBits = 0x0001;
    categoryBits = 0x0001;
    collisionGroup = 0;
    
    box = new FBox(size.x, size.y);

    box.setDrawable(false);
    box.setStatic(true);
    box.setRestitution(0.5);
    box.setFriction(0);
    box.setGrabbable(false);
    box.setGroupIndex(collisionGroup);
    box.setFilterBits(maskBits);
    box.setCategoryBits(categoryBits);

    setPosition(px, py);
  }
  
  void render() {
    push();
    
    rectMode(CENTER);
    fill(colour);
    
    translate(position.x, position.y);
    rect(0, 0, size.x, size.y);
    
    pop();
  }
  
void renderOffscreen(PGraphics p) {
   p.push();
   
   p.rectMode(CENTER);
   p.fill(colour);
   p.noStroke();
   
   p.translate(position.x, position.y);
   p.rect(0, 0, size.x, size.y);
   
   p.pop();
  }

  void setPosition(float x, float y) {
    position.set(x, y);
    box.setPosition(position.x, position.y);
  }

  void setSize(float x, float y) {
    size.set(x, y);
    box.setWidth(size.x);
    box.setHeight(size.y);
  }

  public PVector getPosition() {
    return position;
  }

  public PVector getSize() {
    return size;
  }

  public FBody getBox() {
    return box;
  }

  public void setCollisionGroup(int g) {
    // https://www.iforce2d.net/b2dtut/collision-filtering
    collisionGroup = g;

    box.setGroupIndex(collisionGroup);
  }

  public void setCategoryBits(int b) {
    categoryBits = b;

    box.setCategoryBits(categoryBits);
  }
  public void setMaskBits(int b) {
    maskBits = b;

    box.setFilterBits(maskBits);
  }
  
  public void setColor(color c) {
   colour = c; 
  }
}
