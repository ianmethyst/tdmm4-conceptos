class EnfermedadScene extends Scene {
  Entity infected;

  public EnfermedadScene() {
    super();

    int size = 30;

    for (int i = 0; i < 10; i++) {
      entities.add(new Entity()
        .setPosition(random(left + size * 2, right - size * 2), random(top + size * 2, bottom - size * 2))
        .setVertices(5)
        .setSize(size));

      entities.get(i).getBody().setName("");
    }

    infect(entities.get(0));

    addAll();
  }

  void update() {
    super.update();

    for (Entity e : entities) {
      if (e.getBody().getName().equals("infected")) {
        if (e.getEnfermedadLerpValue() < 1) {
          e.setEnfermedadLerpValue(e.getEnfermedadLerpValue() + 0.01);
          e.setColor(lerpColor(PRIMARY, ACCENT, e.getEnfermedadLerpValue()));
        } else if (e.getEnfermedadLerpValue() >= 1) {
          e.setEnfermedadLerpValue(1);
          e.setColor(ACCENT);
          e.setGrabbable(true);
        }

        continue;
      }


      for (Entity inf : entities) {
        if (inf.getBody().getName().equals("infected")) {
          repelEntity(e, inf);
        }
      }

      ArrayList<FContact> contacts = e.getBody().getContacts();

      for (FContact c : contacts) {
        if (c.getBody1().getName() != null && c.getBody2().getName() != null) {
          if (c.getBody1().getName().equals("infected") || c.getBody2().getName().equals("infected")) {
            infect(e);
          }
        }
      }
    }
  }

  void infect(Entity e) {
    e.getBody().setName("infected");
  }

  void repelEntity(Entity e, Entity inf) {
    PVector position = e.getPosition(); 
    PVector acceleration = PVector.sub(position, inf.getPosition());
    // Set magnitude of acceleration
    acceleration.mult(10);

    //e.body.setTransform(e.body.getWorldCenter(), atan2(target.y, target.x));
    e.getBody().addTorque(acceleration.heading());
    e.getBody().addForce(acceleration.x, acceleration.y);
  }
}
