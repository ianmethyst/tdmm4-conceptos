import java.util.ListIterator;
import java.lang.reflect.*;

import fisica.*;

boolean debugging = false;

SceneManager sm;

boolean windowed = true;
float RATIO = 4.0/3.0;
int HEIGHT = 600;
int WIDTH = round(HEIGHT * RATIO);

void settings() {
  if (windowed) {
    size(WIDTH, HEIGHT, P2D);
  } else {
    fullScreen(P2D);
  }

  smooth(16);
}

void setup() {
  surface.setTitle("floating");
  Fisica.init(this);  
  noStroke();
}

void draw() {
  if (sm == null && height == HEIGHT) {
    sm = new SceneManager();

    //sm.setOverview(false);
    //sm.setCurrentScene(11);
  }



  background(BG);

  if (sm != null) {
    sm.update();
    sm.display();
  }
}

void keyReleased() {
  if (sm != null) {
    switch (key) {
    case 'n': 
      sm.selectNextScene();
      break;

    case 'p': 
      sm.selectPreviousScene();
      break;

    case 'r':
      boolean state = sm.getOverview();
      int index = sm.getCurrentScene(); 
      sm = new SceneManager();
      sm.setOverview(state);
      sm.setCurrentScene(index);
      break;

    case 'd':
      debugging = !debugging;
      break;

    case ' ':
      sm.toggleOverview();
      break;
    }
  }
}

void mousePressed() {
  if (sm != null) {
    if (sm.getOverview()) {
      sm.toggleOverview();
    }
  }
}
