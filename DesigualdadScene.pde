class DesigualdadScene extends Scene {
  Entity l, r;

  Wall rtWall, rrWall, rbWall;

  public DesigualdadScene() {
    super();

    // Right
    rtWall = new Wall(width * 0.75, height * 0.20, width * 0.375, height * 0.4);
    rbWall = new Wall(width * 0.75, height * 0.80, width * 0.375, height * 0.4);
    rrWall = new Wall(right - boundariesWidth, height / 2, boundariesWidth * 2, height);

    rtWall.setColor(ACCENT);
    rbWall.setColor(ACCENT);

    rtWall.getBox().setGrabbable(true);
    rbWall.getBox().setGrabbable(true);

    addWall(rtWall);
    addWall(rbWall);
    addWall(rrWall);

    walls.add(new Wall(width / 2, boundariesWidth / 2, width, boundariesWidth));
    walls.add(new Wall(width / 2, height - boundariesWidth / 2, width, boundariesWidth));

    r = new Entity().setPosition(width * 0.75, height * 0.5).setMinVelocity(100);
    entities.add(r);

    // Left 
    Wall ltWall = new Wall(width * 0.25, height * 0.20, width * 0.4, height * 0.4);
    Wall lbWall = new Wall(width * 0.25, height * 0.80, width * 0.4, height * 0.4);
    Wall llWall = new Wall(left + boundariesWidth, height / 2, boundariesWidth * 2, height);

    addWall(ltWall);
    addWall(lbWall);
    addWall(llWall);

    l = new Entity().setPosition(width * 0.25, height * 0.5).setMinVelocity(100);
    entities.add(l);

    // Center
    Wall lrWall = new Wall(width * 0.5, height / 2, boundariesWidth * 2, height);
    addWall(lrWall);

    //addAll();
  }

  public void update() {
    super.update();

    float rtY = rtWall.getBox().getY();

    if (rtY < height * 0.01) {
      rtWall.setPosition(width * 0.75, height * 0.01);
    } else if (rtY > height * 0.20) {
      rtWall.setPosition(width * 0.75, height * 0.20);
    } else {
      rtWall.setPosition(width * 0.75, rtY);
    }

    float rbY = rbWall.getBox().getY();

    if (rbY > height * 0.99) {
      rbWall.setPosition(width * 0.75, height * 0.99);
    } else if (rbY < height * 0.80) {
      rbWall.setPosition(width * 0.75, height * 0.80);
    } else {
      rbWall.setPosition(width * 0.75, rbY);
    }
  }
}
